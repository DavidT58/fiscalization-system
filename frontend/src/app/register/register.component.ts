import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
  }

  firstName = "";
  lastName = "";
  username = "";
  email = "";
  phone = "";
  businessName = "";
  businessAddress = "";
  pib = "";
  companyNumber = "";
  password = "";
  passwordConfirmation = "";
  errorMessages = [];

  // imageOK = false;
  notEmpty = false;
  passwordOK = false;
  pibOK = false;


  image: File = null;

  

  register(){

    // let user = new User();
    this.errorMessages = [];

    const regexPIB = /^[1-9][0-9]{8}$/;
    const regexPassword = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,12}$/;

    if(this.firstName == "" || this.lastName == "" ||
      this.username == "" || this.email == "" ||
      this.phone == "" || this.businessName == "" ||
      this.businessAddress == "" || this.pib == "" ||
      this.companyNumber == "" || this.password == "" ||
      this.passwordConfirmation == ""
    ){
      this.errorMessages.push("Sva polja moraju biti popunjena!");
      this.notEmpty = false;
    }
    else{
      this.notEmpty = true;
    }

    if(this.password != this.passwordConfirmation){
      this.errorMessages.push("Lozinke se ne podudaraju!");
      this.passwordOK = false;
    }
    else{
      this.passwordOK = true;
    }

    if(!this.pib.match(regexPIB)){
      this.errorMessages.push("PIB nije u ispravnom formatu!");
      this.pibOK = false;
    }
    else{
      this.pibOK = true;
    }

    if(!this.password.match(regexPassword)){
      this.errorMessages.push("Lozinka nije u ispravnom formatu!");
      this.passwordOK = false;
    }
    else{
      this.passwordOK = true;
    }

    if(this.image){
      var _URL = window.URL || window.webkitURL;
      var img = new Image();
      var objectUrl = _URL.createObjectURL(this.image);
      img.onload = () => {
        if(img.width > 300 || img.height > 300 || img.width < 100 || img.height < 100){
          this.errorMessages.push("Neispravna slika!");
        }
        else{
          if(this.passwordOK && this.pibOK && this.notEmpty){
            const data: FormData = new FormData();
      
            data.append("firstName", this.firstName);
            data.append("lastName", this.lastName);
            data.append("username", this.username);
            data.append("email", this.email);
            data.append("phone", this.phone);
            data.append("businessName", this.businessName);
            data.append("businessAddress", this.businessAddress);
            data.append("pib", this.pib);
            data.append("companyNumber", this.companyNumber);
            data.append("password", this.password);
            data.append("image", this.image);
            
      
            this.usersService.register(data).subscribe((resp: any) => {
              if(resp.username){
                localStorage.setItem('user', JSON.stringify(resp));
                this.router.navigateByUrl('/');
                window.location.reload();
              }
              else if(resp.exists){
                alert("Postoji vec korisnik sa izabranim korisnickim imenom ili mejlom");
                this.router.navigateByUrl('/login');
              }
            })
          }
        }
        
        _URL.revokeObjectURL(objectUrl);
      }
      img.src = objectUrl;
    }
    else{
      this.errorMessages.push("Ne postoji slika!");
    }
  }


  uploadImage(event: any){
    this.image = event.target.files[0];
  }

}
