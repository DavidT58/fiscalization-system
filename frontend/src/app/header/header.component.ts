import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) {
    
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.loggedIn = (this.user) ? true : false;
  }

  loggedIn: boolean;
  user: any;

  logout(){
    this.loggedIn = false;
    this.user = null;
    localStorage.removeItem("user");
    localStorage.removeItem("tables");
    // window.location.reload();
    this.router.navigate(['/']).then( () => {
      window.location.reload();
    });
  }

}
