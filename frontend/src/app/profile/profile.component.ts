import { HttpRequest, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import activityCodes from '../models/activityCodes';
import { BankAccount } from '../models/bankAccount';
import { FiscalRegister } from '../models/fiscalRegister';
import { Warehouse } from '../models/warehouse';
import { CompanyService } from '../services/company.service';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private usersService: UsersService, private router: Router, private companyService: CompanyService) { }

  user: any;

  kategorija = "";
  sifre = activityCodes;
  sifreDelatnosti = [];
  pdv: boolean = false;
  racuni = Array<BankAccount>();
  magacini = Array<Warehouse>();
  kase = Array<FiscalRegister>();

  notEmpty = true;
  bankAccountsOK = true;
  warehousesOK = true;
  registersOK = true;

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));

    if(this.user.category){
      this.kategorija = this.user.category;
      this.sifreDelatnosti = this.user.activityCodes;
      this.pdv = this.user.pdv;
      this.racuni = this.user.bankAccounts;
      this.magacini = this.user.warehouses;
      this.kase = this.user.fiscalRegisters;
    }
  }

  oldPassword = "";
  newPassword = "";
  confirmPassword = "";

  passwordOK = false;

  errorMessages;

  addBankAccount(){
    let ba = new BankAccount();
    ba.accountNumber = "";
    ba.bankName = "";
    this.racuni.push(ba);
  }

  removeBankAccount(i){
    this.racuni.splice(i, 1);
  }

  addWarehouse(){
    let w = new Warehouse();
    w.warehouseID = 0;
    w.warehouseName = "";
    w.company = this.user.username;
    this.magacini.push(w);
  }

  removeWarehouse(i){
    let wh = this.magacini.splice(i, 1)[0];
    let data = {
      warehouseID: wh.warehouseID
    }
    this.companyService.removeWarehouse(this.user.username, data).subscribe( (resp: any) => {
      if(resp.companyNotFound){
        this.errorMessages.push("ERROR");
      }
      else if(resp){
        this.user = resp;
        this.magacini = this.user.warehouses;
        localStorage.setItem("user", JSON.stringify(resp));
      }
    });
  }

  addRegister(){
    let r = new FiscalRegister();
    r.registerType = "";
    r.objectLocation = "";
    this.kase.push(r);
  }

  removeRegister(i){
    this.kase.splice(i, 1);
  }

  submitCompanyDetails(){
    // console.log(this.sifreDelatnosti);
    this.errorMessages = [];

    this.notEmpty = true;
    this.bankAccountsOK = true;
    this.warehousesOK = true;
    this.registersOK = true;

    const regexBankAccount = /[0-9]{3}-[0-9]{12}-[0-9]{2}/;

    if(this.kategorija == "" || this.sifreDelatnosti.length < 1 || 
      this.racuni.length < 1 || this.magacini.length < 1 || this.kase.length < 1){
        this.errorMessages.push("Sva polja moraju biti popunjena!");
        this.notEmpty = false;
      }

    this.racuni.forEach((value) => {
      if(value.accountNumber == "" || value.bankName == "" || !regexBankAccount.test(value.accountNumber.toString())){
        this.errorMessages.push("Neispravni bankovni racuni!");
        this.bankAccountsOK = false;
      }
    });

    this.magacini.forEach((value) => {
      if(value.warehouseID < 1 || value.warehouseName == ""){
        this.errorMessages.push("Neispravni magacini!");
        this.warehousesOK = false;
      }
    })

    this.kase.forEach((value) => {
      if(value.objectLocation == "" || value.registerType == ""){
        this.errorMessages.push("Neispravne kase!");
        this.registersOK = false;
      }
    })

    if(this.notEmpty && this.bankAccountsOK && this.warehousesOK && this.registersOK){
      let data = {
        username: this.user.username,
        category: this.kategorija,
        activityCodes: this.sifreDelatnosti,
        pdv: this.pdv,
        bankAccounts: this.racuni,
        warehouses: this.magacini,
        fiscalRegisters: this.kase
      }
      this.companyService.submitCompanyDetails(data).subscribe((resp: any) => {
        if(resp.companyNotFound){
          this.errorMessages.push("Ne postoji preduzece!");
        }
        else{
          this.user = resp;
          localStorage.setItem("user", JSON.stringify(resp));
          window.location.reload();
        }
      })
    }
  }

  changePass(){
    this.errorMessages = [];

    const regexPassword = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,12}$/;
    
    if(this.oldPassword == "" || this.newPassword == "" || this.confirmPassword == ""){
      this.errorMessages.push("Sva polja moraju biti popunjena!");
      this.passwordOK = false;
    }
    else if(this.newPassword != this.confirmPassword){
      this.errorMessages.push("Potvrda lozinke se ne podudara!");
      this.passwordOK = false;
    }
    else if(this.oldPassword == this.newPassword){
      this.errorMessages.push("Nova lozinka ne sme biti ista kao i stara!");
      this.passwordOK = false;
    }
    else if(!this.newPassword.match(regexPassword)){
      this.errorMessages.push("Lozinka nije u ispravnom formatu!");
      this.passwordOK = false;
    }
    else{
      this.passwordOK = true;
    }

    if(this.passwordOK){
      
      let data = {
        "username": this.user.username,
        "oldPassword": this.oldPassword,
        "newPassword": this.newPassword,
        "type": this.user.type
      }

      this.usersService.resetPassword(data).subscribe( (resp: any) => {
        if(resp.oldPasswordCorrect){
          alert("Uspesno ste resetovali lozinku!");
          if(this.user.type == "Admin"){
            this.router.navigate(['/admin/login']).then( () => {
              window.location.reload();
            });
          }
          else{
            this.router.navigate(['/login']).then( () => {
              window.location.reload();
            });
          }
          localStorage.removeItem("user");
        }
        else{
          this.errorMessages.push("Neispravna stara lozinka!");
        }
      })
    } 
  }

}
