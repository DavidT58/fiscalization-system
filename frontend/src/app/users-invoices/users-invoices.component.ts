import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-users-invoices',
  templateUrl: './users-invoices.component.html',
  styleUrls: ['./users-invoices.component.scss']
})
export class UsersInvoicesComponent implements OnInit {

  user;
  invoices;

  currentInvoice;

  previewInProgress = false;

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.companyService.getUserInvoices(this.user.username).subscribe( (resp) => {
      this.invoices = resp;
      // console.log(resp);
    })
  }

  previewInvoice(invoice){
    this.previewInProgress = true;
    this.currentInvoice = invoice;
  }

  back(){
    this.previewInProgress = false;
  }

}
