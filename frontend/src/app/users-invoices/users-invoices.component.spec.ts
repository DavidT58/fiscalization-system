import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersInvoicesComponent } from './users-invoices.component';

describe('UsersInvoicesComponent', () => {
  let component: UsersInvoicesComponent;
  let fixture: ComponentFixture<UsersInvoicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersInvoicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
