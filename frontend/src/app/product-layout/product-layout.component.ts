import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ProductLayoutDialogComponent } from '../product-layout-dialog/product-layout-dialog.component';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-product-layout',
  templateUrl: './product-layout.component.html',
  styleUrls: ['./product-layout.component.scss']
})
export class ProductLayoutComponent implements OnInit {

  constructor(private companyService: CompanyService, private dialog: MatDialog) {
    this.companyService.getCategories().subscribe( (resp : Array<Object>) => {
      this.categories = resp;
    })
  }

  ngOnInit(): void {
  }

  categories = [];
  categoryName = "";
  subCategoryName = "";

  addCategory(){

    if(this.categoryName != ""){
      const data = {
        categoryName: this.categoryName
      }
      this.companyService.addCategory(data).subscribe( (resp : any) => {
        this.categories = resp;
      })
    }
    else{
      alert("Kategorija mora imati ime!");
    }
  }

  addSubCategory(category){
    const data = {
      subCategoryName: this.subCategoryName
    }
    this.companyService.addSubCategory(category, data).subscribe( (resp: any) => {
      this.categories = resp;
    })
  }

  openDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.height = 'auto';
    dialogConfig.width = 'auto';

    this.dialog.open(ProductLayoutDialogComponent, dialogConfig);
  }

}
