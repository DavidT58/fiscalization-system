import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-admin-users-view',
  templateUrl: './admin-users-view.component.html',
  styleUrls: ['./admin-users-view.component.scss']
})
export class AdminUsersViewComponent implements OnInit {

  pendingCompanies: any[];
  allCompanies: any[];


  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
    this.adminService.getPendingCompanies().subscribe((resp) => {
      if(resp.length != 0){
        // console.log(resp);
        // resp.forEach(function(company){
        //   console.log(company.username, company.pib);
        // })
        this.pendingCompanies = resp;
      }
      else{
        console.log("No pending companies");
      }
    })

    this.adminService.getCompanies().subscribe((resp) => {
      if(resp.length != 0){
        // console.log(resp);
        // resp.forEach(function(company){
        //   console.log(company.username, company.pib);
        // })
        this.allCompanies = resp;
      }
      else{
        console.log("No rejected companies");
      }
    })
  }

  approveCompany(username: string){
    // console.log(username);
    this.adminService.approveCompany(username).subscribe((resp) => {
      console.log(resp);
      window.location.reload();
    })
  }

  rejectCompany(username: string){
    // console.log(username);
    this.adminService.rejectCompany(username).subscribe((resp) => {
      console.log(resp);
      window.location.reload();
    })
  }

  username = "";
  password = "";
  confirmPassword = "";
  firstName = "";
  lastName = "";
  idNumber = "";
  phone = "";
  email = "";
  businessName = "";
  businessAddress = "";
  pib = "";
  companyNumber = "";

  image: File = null;

  type = "company";

  errorMessages = [];

  notEmpty = false;
  passwordOK = false;
  pibOK = false;

  regexPIB = /^[1-9][0-9]{8}$/;
  regexPassword = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,12}$/;


  registerCompany(){

    this.errorMessages = [];

    if(this.firstName == "" || this.lastName == "" ||
      this.username == "" || this.email == "" ||
      this.phone == "" || this.businessName == "" ||
      this.businessAddress == "" || this.pib == "" ||
      this.companyNumber == "" || this.password == "" ||
      this.confirmPassword == ""
    ){
      this.errorMessages.push("Sva polja moraju biti popunjena!");
      this.notEmpty = false;
    }
    else{
      this.notEmpty = true;
    }

    if(this.password != this.confirmPassword){
      this.errorMessages.push("Lozinke se ne podudaraju!");
      this.passwordOK = false;
    }
    else{
      this.passwordOK = true;
    }

    if(!this.pib.match(this.regexPIB)){
      this.errorMessages.push("PIB nije u ispravnom formatu!");
      this.pibOK = false;
    }
    else{
      this.pibOK = true;
    }

    if(!this.password.match(this.regexPassword)){
      this.errorMessages.push("Lozinka nije u ispravnom formatu!");
      this.passwordOK = false;
    }
    else{
      this.passwordOK = true;
    }

    if(this.image){
      var _URL = window.URL || window.webkitURL;
      var img = new Image();
      var objectUrl = _URL.createObjectURL(this.image);
      img.onload = () => {
        if(img.width > 300 || img.height > 300 || img.width < 100 || img.height < 100){
          this.errorMessages.push("Neispravna slika!");
        }
        else if(this.passwordOK && this.pibOK && this.notEmpty){
          const data: FormData = new FormData();
    
          data.append("firstName", this.firstName);
          data.append("lastName", this.lastName);
          data.append("username", this.username);
          data.append("email", this.email);
          data.append("phone", this.phone);
          data.append("businessName", this.businessName);
          data.append("businessAddress", this.businessAddress);
          data.append("pib", this.pib);
          data.append("companyNumber", this.companyNumber);
          data.append("password", this.password);
          data.append("image", this.image);
          
    
          this.adminService.registerCompany(data).subscribe((resp: any) => {
            if(resp.registered){
              // localStorage.setItem('user', JSON.stringify(resp));
              // this.router.navigateByUrl('/');
              // window.location.reload();
              alert("Korisnik uspesno registrovan!");
            }
            else if(resp.exists){
              alert("Postoji vec korisnik sa izabranim korisnickim imenom ili mejlom");
              // this.router.navigateByUrl('/login');
            }
          })
        }
        
        _URL.revokeObjectURL(objectUrl);
      }
      img.src = objectUrl;
    }
    else{
      this.errorMessages.push("Ne postoji slika!");
    }
  }

  registerUser(){
    this.errorMessages = [];

    if(this.firstName == "" || this.lastName == "" ||
      this.username == "" || this.phone == "" || 
      this.idNumber == "" || this.password == "" ||
      this.confirmPassword == ""
    ){
      this.errorMessages.push("Sva polja moraju biti popunjena!");
      this.notEmpty = false;
    }
    else{
      this.notEmpty = true;
    }

    if(this.password != this.confirmPassword){
      this.errorMessages.push("Lozinke se ne podudaraju!");
      this.passwordOK = false;
    }
    else{
      this.passwordOK = true;
    }

    if(!this.password.match(this.regexPassword)){
      this.errorMessages.push("Lozinka nije u ispravnom formatu!");
      this.passwordOK = false;
    }
    else{
      this.passwordOK = true;
    }

    if(this.passwordOK && this.notEmpty){
      
      let data = {
        firstName: this.firstName,
        lastName: this.lastName,
        username: this.username,
        password: this.password,
        phone: this.phone,
        idNumber: this.idNumber
      }

      this.adminService.registerUser(data).subscribe( (resp: any) => {
        console.log(resp);
        if(resp.registered){
          // localStorage.setItem('user', JSON.stringify(resp));
          // this.router.navigateByUrl('/');
          // window.location.reload();
          alert("Korisnik uspesno registrovan!");
        }
        else if(resp.exists){
          alert("Postoji vec korisnik sa izabranim korisnickim imenom ili mejlom");
          // this.router.navigateByUrl('/login');
        }
      })

    }
  }

  uploadImage(event: any){
    this.image = event.target.files[0];
  }

}
