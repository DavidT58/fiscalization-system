import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  error: string;

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
  }

  login(){

    this.error = "";

    if(this.username && this.password){
      let data = {
        "username": this.username,
        "password": this.password
      }
      this.usersService.login(data).subscribe((resp: any) => {
        if(resp.username){
          localStorage.setItem("user", JSON.stringify(resp));
          this.router.navigate(['/profile']).then( () => {
            window.location.reload();
          });
        }
        else{
          this.error = "Ne postoji potvrdjen korisnik sa tim kredencijalima";
        }
      })
    }
    else{
      this.error = "Morate uneti oba polja!";
    }
  }
}
