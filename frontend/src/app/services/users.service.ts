import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiUrl = 'http://localhost:8000';

  constructor(private http: HttpClient) { }

  register(data){
    return this.http.post(`${this.apiUrl}/users/register`, data);
  }

  login(data){
    return this.http.post(`${this.apiUrl}/users/login`, data);
  }

  adminLogin(data){
    return this.http.post(`${this.apiUrl}/users/adminLogin`, data);
  }

  resetPassword(data){
    return this.http.post(`${this.apiUrl}/users/resetPassword`, data);
  }
  
}
