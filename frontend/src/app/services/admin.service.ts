import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  apiUrl = 'http://localhost:8000';

  constructor(private http: HttpClient) { }

  getPendingCompanies(){
    return this.http.get<any[]>(`${this.apiUrl}/admin/getPendingCompanies`);
  }

  getCompanies(){
    return this.http.get<any[]>(`${this.apiUrl}/admin/getCompanies`);
  }

  approveCompany(username: string){
    return this.http.post(`${this.apiUrl}/admin/approveCompany/${username}`, {});
  }

  rejectCompany(username: string){
    return this.http.post(`${this.apiUrl}/admin/rejectCompany/${username}`, {});
  }

  registerCompany(data){
    return this.http.post(`${this.apiUrl}/admin/registerCompany`, data);
  }

  registerUser(data){
    return this.http.post(`${this.apiUrl}/admin/registerUser`, data);
  }

}
