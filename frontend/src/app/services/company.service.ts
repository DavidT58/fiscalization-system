import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  apiUrl = 'http://localhost:8000';

  submitCompanyDetails(data){
    return this.http.post(`${this.apiUrl}/companies/submitCompanyDetails`, data);
  }

  getCompanyByPIB(pib){
    return this.http.get(`${this.apiUrl}/companies/getCompanyByPIB/${pib}`);
  }

  getCompanyByID(id){
    return this.http.get(`${this.apiUrl}/companies/getCompanyByID/${id}`);
  }

  getOrdererByID(id){
    return this.http.get(`${this.apiUrl}/companies/getOrdererByID/${id}`);
  }

  addOrdererToCompany(data){
    return this.http.post(`${this.apiUrl}/companies/addOrdererToCompany`, data);
  }

  addWarehouse(company, data){
    return this.http.post(`${this.apiUrl}/companies/${company}/addWarehouse`, data);
  }

  removeWarehouse(company, data){
    return this.http.post(`${this.apiUrl}/companies/${company}/removeWarehouse`, data);
  }

  getCompanyProducts(company){
    return this.http.get(`${this.apiUrl}/companies/${company}/getCompanyProducts`);
  }

  addProduct(company, data){
    return this.http.post(`${this.apiUrl}/companies/${company}/addProduct`, data);
  }

  getCategories(){
    return this.http.get(`${this.apiUrl}/companies/getCategories`);
  }

  addCategory(data){
    return this.http.post(`${this.apiUrl}/companies/addCategory`, data);
  }

  addSubCategory(category, data){
    return this.http.post(`${this.apiUrl}/companies/categories/${category}/addSubCategory`, data);
  }

  getCompanyProductsByName(company, searchTerm){
    return this.http.get(`${this.apiUrl}/companies/${company}/getCompanyProductsByName/${searchTerm}`);
  }

  addCategoryToProduct(data){
    return this.http.post(`${this.apiUrl}/companies/addCategoryToProduct`, data);
  }

  getCompanyWarehouses(company){
    return this.http.get(`${this.apiUrl}/companies/${company}/getCompanyWarehouses`);
  }

  generateInvoice(company, data){
    return this.http.post(`${this.apiUrl}/companies/${company}/generateInvoice`, data);
  }

  updateTables(company, data){
    return this.http.post(`${this.apiUrl}/companies/${company}/updateTables`, data);
  }

  getReport(company, data){
    return this.http.post(`${this.apiUrl}/companies/${company}/getReport`, data);
  }

  getCompanies(){
    return this.http.get(`${this.apiUrl}/companies`);
  }

  getUserInvoices(username){
    return this.http.get(`${this.apiUrl}/companies/getUserInvoices/${username}`);
  }

  getProducts(){
    return this.http.get(`${this.apiUrl}/companies/products`);
  }

  getInvoices(){
    return this.http.get(`${this.apiUrl}/companies/invoices`);
  }

}
