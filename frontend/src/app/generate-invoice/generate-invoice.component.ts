import { Component, OnInit } from '@angular/core';
import { Invoice } from '../models/invoice';
import { Product } from '../models/product';
import { Table } from '../models/table';
import { CompanyService } from '../services/company.service';


@Component({
  selector: 'app-generate-invoice',
  templateUrl: './generate-invoice.component.html',
  styleUrls: ['./generate-invoice.component.scss']
})
export class GenerateInvoiceComponent implements OnInit {

  constructor(private companyService : CompanyService) { }

  tables;
  currentTable;

  products = [];
  warehouses = [];
  company;
  selectedWarehouse;

  inputsProduct = [];

  counter = 0;

  invoiceInProgress = true;

  invoiceInProgressTable = false;
  paymentScreenTable = false;

  paymentMethods = ["Gotovina", "Ček", "Kartica", "Virmansko"];
  paymentMethod = "";

  total = 0;

  cashGiven = 0;
  idCardNumber = "";
  change = 0;

  customerFirstName = "";
  customerLastName = "";

  slipNumber= "";

  orderer = "";
  orderers = [];

  errors = [];

  ok = false;

  ngOnInit(): void {
    this.company = JSON.parse(localStorage.getItem("user"));
    this.orderers = this.company['orderers']; 
    this.companyService.getCompanyWarehouses(this.company.username).subscribe( (resp : any) => {
      this.warehouses = resp;
    })

    if(this.company.category == 'Ugostiteljski objekat'){
      this.tables = JSON.parse(localStorage.getItem('tables'));
      if(this.tables == undefined){
        this.tables = [];
        this.company.tables.forEach( (table) => {
          let t = new Table();
          t.name = table.name;
          t.status = table.status;
          t.total = 0;
          t.productsOnBill = [];
          this.tables.push(t);
        });
        localStorage.setItem('tables', JSON.stringify(this.tables));
      }
    }

    this.companyService.getCompanyProducts(this.company.username).subscribe( (resp: any) => {
      this.getProductsFromResponse(resp);
    })
  }

  removeItem(i){
    if(this.company.category == 'Ugostiteljski objekat'){
      this.tables[this.currentTable].productsOnBill.splice(i, 1);
      localStorage.setItem('tables', JSON.stringify(this.tables));
    }
    else{
      this.inputsProduct.splice(i, 1);
    }
  }

  addItem(){
    let p = new Product();
    if(this.company.category == 'Ugostiteljski objekat'){
      this.tables[this.currentTable].productsOnBill.push(p);
      localStorage.setItem('tables', JSON.stringify(this.tables));
    }
    else{
      this.inputsProduct.push(p);
    }
  }

  updateProducts(){
    this.companyService.getCompanyProducts(this.company.username).subscribe( (resp: any) => {
      this.getProductsFromResponse(resp);
      this.products = this.products.filter( product => 
        product.warehouse == this.selectedWarehouse
      )
    })
  }

  getProductsFromResponse(resp){
    this.products = [];
    resp.forEach(element => {
      element.product.currentStock = element.currentStock;
      this.products.push(element.product);
    });
  }

  closeInvoice(){

    const unique = (value, index, self) => {
      return self.indexOf(value) === index
    }

    if(this.company.category == 'Ugostiteljski objekat'){
      this.tables[this.currentTable].total = 0;
      this.tables[this.currentTable].productsOnBill.forEach( product => {
        let t = this.products.find( p => p._id == product._id);
        this.tables[this.currentTable].total += (t.salesPriceRSD * product.amount);
      })
      this.paymentScreenTable = true;
      this.invoiceInProgressTable = false;
    }
    else{
      this.invoiceInProgress = false;
      this.total = 0;
      
      this.inputsProduct = this.inputsProduct.filter(unique);

      this.inputsProduct.forEach(i => {
        this.total += (i.salesPriceRSD * i.amount);
      });
    }
  }

  calculateChange(){
    if(this.company.category == 'Ugostiteljski objekat'){
      this.change = this.cashGiven - this.tables[this.currentTable].total;
    }
    else{
      this.change = this.cashGiven - this.total;
    }
  }

  cancel(){
    if(this.company.category == 'Ugostiteljski objekat'){
      this.paymentScreenTable = false;
      this.invoiceInProgressTable = true;
    }
    else{
      this.invoiceInProgress = true;
    }
    
    this.errors = [];
    this.updateProducts();
  }

  generateInvoice(){
    
    this.errors = [];

    if(this.paymentMethod == 'Ček' && (this.customerFirstName == "" || this.customerLastName == "")){
      this.errors.push("Morate uneti ime, prezime i broj lične karte kupca i slip-a za ovaj tip plaćanja!");
      this.ok = false;
    }
    else if(this.paymentMethod == 'Gotovina' && this.total > this.cashGiven){
      this.errors.push("Kupac nije dao dovoljno sredstava!");
      this.ok = false;
    }
    else if(this.paymentMethod == 'Kartica' && (this.slipNumber == "" || this.idCardNumber == "")){
      this.errors.push("Morate uneti broj lične karte kupca i broj slip-a za ovaj tip plaćanja!");
      this.ok = false;
    }
    else if (this.paymentMethod == 'Virmansko' && this.orderer == ""){
      this.errors.push("Morate uneti naručioca za ovaj tip plaćanja!");
      this.ok = false;
    }
    else if(this.paymentMethod == ''){
      this.errors.push("Morate izabrati tip plaćanja!");
      this.ok = false;
    }
    else{
      this.ok = true;
    }

    if(this.inputsProduct.length < 1){
      this.ok = false;
      this.errors.push("Morate uneti bar 1 proizvod!");
    }

    this.inputsProduct.forEach( prod => {
      if(prod.amount > prod.currentStock){
        this.ok = false;
        this.errors.push("Nema dovoljno " + prod.productName +" na stanju!");
      }
      if(prod.amount < 1){
        this.ok = false;
        this.errors.push("Količina proizvoda ne sme biti 0!");
      }
    })
    
    if(this.ok){
      let inv = new Invoice();

      inv.company = this.company.username;
      inv.dateCreated = new Date().toISOString();

      inv.paymentMethod = this.paymentMethod;

      inv.total = this.total;

      inv.products = this.inputsProduct;

      inv.idCardNumber = this.idCardNumber;

      switch(this.paymentMethod){
        case 'Ček':
          inv.customerFirstName = this.customerFirstName;
          inv.customerLastName = this.customerLastName;
          break;
        case 'Kartica':
          inv.slipNumber = this.slipNumber;
          break;
        case 'Virmansko':
          inv.orderer = this.orderer;
          break;
      }

      let t : any = inv;
      t.warehouseId = this.selectedWarehouse;

      this.companyService.generateInvoice(this.company.username, t).subscribe( (resp: any) => {
        if(resp){
          if(resp.company){
            localStorage.setItem("user", JSON.stringify(resp.company))
            this.company = JSON.parse(localStorage.getItem("user"));
          }
          if(resp.products){
            this.getProductsFromResponse(resp.products);
          }
          if(resp.success){
            window.location.reload();
          }
          else{
            this.errors.push("Doslo je do greske...");
          }
        }
      })
    }
  }

  generateInvoiceForTable(){
    this.errors = [];

    if(this.paymentMethod == 'Ček' && (this.customerFirstName == "" || this.customerLastName == "")){
      this.errors.push("Morate uneti ime, prezime i broj lične karte kupca i slip-a za ovaj tip plaćanja!");
      this.ok = false;
    }
    else if(this.paymentMethod == 'Gotovina' && this.tables[this.currentTable].total > this.cashGiven){
      this.errors.push("Kupac nije dao dovoljno sredstava!");
      this.ok = false;
    }
    else if(this.paymentMethod == 'Kartica' && (this.slipNumber == "" || this.idCardNumber == "")){
      this.errors.push("Morate uneti broj lične karte kupca i broj slip-a za ovaj tip plaćanja!");
      this.ok = false;
    }
    else if (this.paymentMethod == 'Virmansko' && this.orderer == ""){
      this.errors.push("Morate uneti naručioca za ovaj tip plaćanja!");
      this.ok = false;
    }
    else if(this.paymentMethod == ''){
      this.errors.push("Morate izabrati tip plaćanja!");
      this.ok = false;
    }
    else{
      this.ok = true;
    }

    if(this.tables[this.currentTable].productsOnBill.length < 1){
      this.ok = false;
      this.errors.push("Morate uneti bar 1 proizvod!");
    }

    this.tables[this.currentTable].productsOnBill.forEach( prod => {
      let t = this.products.find( p => p._id == prod._id);
      if(prod.amount > t.currentStock){
        this.ok = false;
        this.errors.push("Nema dovoljno " + t.productName +" na stanju!");
      }
      if(prod.amount < 1){
        this.ok = false;
        this.errors.push("Količina proizvoda ne sme biti 0!");
      }
    })
    
    if(this.ok){
      let inv = new Invoice();

      inv.company = this.company.username;
      inv.dateCreated = new Date().toISOString();

      inv.paymentMethod = this.paymentMethod;

      inv.total = this.tables[this.currentTable].total;

      inv.products = this.tables[this.currentTable].productsOnBill;

      inv.idCardNumber = this.idCardNumber;

      switch(this.paymentMethod){
        case 'Ček':
          inv.customerFirstName = this.customerFirstName;
          inv.customerLastName = this.customerLastName;
          break;
        case 'Kartica':
          inv.slipNumber = this.slipNumber;
          break;
        case 'Virmansko':
          inv.orderer = this.orderer;
          break;
      }

      let t : any = inv;
      t.warehouseId = this.selectedWarehouse;

      this.companyService.generateInvoice(this.company.username, t).subscribe( (resp: any) => {
        if(resp){
          if(resp.company){
            localStorage.setItem("user", JSON.stringify(resp.company))
            this.company = JSON.parse(localStorage.getItem("user"));
          }
          if(resp.products){
            this.getProductsFromResponse(resp.products);
          }
          if(resp.success){
            this.tables[this.currentTable].status = 'SLOBODAN';
            this.tables[this.currentTable].total = 0;
            this.tables[this.currentTable].productsOnBill = [];
            localStorage.setItem('tables', JSON.stringify(this.tables));
            this.invoiceInProgressTable = false;
            this.currentTable = undefined;
            window.location.reload();
          }
          else{
            this.errors.push("Doslo je do greske...");
          }
        }
      })
    }
  }

  selectTable(t){
    let tableIndex = this.tables.indexOf(t);
    if(this.tables[tableIndex].status == 'SLOBODAN'){
      this.tables[tableIndex].status = 'ZAUZET';
      localStorage.setItem('tables', JSON.stringify(this.tables));
    }

    this.currentTable = this.tables.indexOf(t);

    this.invoiceInProgressTable = true;
  }

  returnFromTable(){
    this.invoiceInProgressTable = false;
  }
  
  cancelInvoiceForTable(){
    this.tables[this.currentTable].status = 'SLOBODAN';
    this.tables[this.currentTable].status.productsOnBill = [];
    localStorage.setItem('tables', JSON.stringify(this.tables));
    this.currentTable = undefined;

    this.invoiceInProgressTable = false;
    
  }
}
