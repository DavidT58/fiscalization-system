import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  username: string;
  password: string;

  error: string;

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
  }

  login(){

    this.error = "";

    if(this.username && this.password){
      let data = {
        "username": this.username,
        "password": this.password
      }
      this.usersService.adminLogin(data).subscribe((resp: any) => {
        if(resp.username && resp.type == 'Admin'){
          localStorage.setItem("user", JSON.stringify(resp));
          this.router.navigate(['/profile']).then( () => {
            window.location.reload();
          });
        }
        else{
          this.error = "Ne postoji administrator sa tim kredencijalima";
        }
      })
    }
    else{
      this.error = "Morate uneti oba polja!";
    }
  }
}
