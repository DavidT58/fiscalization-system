import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CompanyService } from '../services/company.service';



@Component({
  selector: 'app-goods-and-services',
  templateUrl: './goods-and-services.component.html',
  styleUrls: ['./goods-and-services.component.scss']
})
export class GoodsAndServicesComponent implements OnInit {

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    
    this.user = JSON.parse(localStorage.getItem("user"));
    this.companyService.getCompanyProducts(this.user.username).subscribe( (resp: Array<Object>) => {
      this.products = resp;
      this.numOfRows = resp.length;
    });
    this.companyService.getCompanyWarehouses(this.user.username).subscribe( (resp: any) => {
      this.warehouses = resp;
    })
  }

  numOfRows;
  p =  1;

  products = [];
  warehouses = [];
  user;

  errors = [];

  showForm = false;
  buttonText = "Unos artikla";

  productCode = "";
  productName = "";
  unitOfMeasure = "";
  selectedWarehouse;
  taxRates = [
    {value: '20', viewValue: '20%'},
    {value: '10', viewValue: '10%'},
    {value: '0', viewValue: '0%'},
  ];
  taxRate = "";
  productTypes = ["Hrana", "Pice", "Sirovina"];
  productType = "";

  notEmpty = false;

  toggleForm(){
    this.showForm = !this.showForm;
    this.buttonText = (this.showForm) ? "Sakrij" : "Unos artikla";
  }

  addProduct(){

    this.notEmpty = false;
    this.errors = [];

    if(this.productCode != "" && this.productName != "" && 
      this.unitOfMeasure != "" && this.taxRate != ""){
      this.notEmpty = true;
    }
    else{
      this.notEmpty = false;
      this.errors.push("Morate uneti obavezna polja!");
    }

    if(this.notEmpty){
      let data = {
        warehouseID: this.selectedWarehouse.warehouseID,
        company: this.user.username,
  
        productCode: this.productCode,
        productName: this.productName,
        unitOfMeasure: this.unitOfMeasure,
        taxRate: this.taxRate,
  
        purchasePriceRSD: 10,
        salesPriceRSD: 20,
      };
      this.companyService.addProduct(this.user.username, data).subscribe( (resp: any) => {
        if(resp.success){
          if(resp){
            this.products = resp;
            window.location.reload();
          }
        }
      })
    }
  }
}
