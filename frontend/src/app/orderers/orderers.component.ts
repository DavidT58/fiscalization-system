import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-orderers',
  templateUrl: './orderers.component.html',
  styleUrls: ['./orderers.component.scss']
})
export class OrderersComponent implements OnInit {

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));
    // this.user.orderers.foreach(
    //   (orderer) => {
    //     this.orderers.push(this.companyService.getOrdererByID(orderer._id));
    //   }
    // )
  }

  user;

  errorMessages = [];
  pibSearch = "";
  orderersSearch = [];
  orderers;

  formFirstName = "";
  formLastName = "";
  formPhone = "";
  formEmail = "";
  formBusinessName = "";
  formBusinessAddress = "";
  formPIB = "";
  formCompanyNumber = "";

  notEmpty = false;
  pibOK = false;

  getCompanyByPIB(){
    if(this.pibSearch != ""){
      this.companyService.getCompanyByPIB(this.pibSearch).subscribe((resp: Array<any>) => {
        this.orderersSearch = resp;
      })
    }
  }

  addOrdererToCompany(username?){
    let orderer;
    const regexPIB = /^[1-9][0-9]{8}$/;

    this.notEmpty = true;
    this.pibOK = true;
    this.errorMessages = [];

    if(username == undefined){
      

      if(
        this.formFirstName == "" ||
        this.formLastName == "" ||
        this.formPhone == "" ||
        this.formEmail == "" ||
        this.formBusinessName == "" ||
        this.formBusinessAddress == "" ||
        this.formPIB == "" ||
        this.formCompanyNumber == ""
      ){
        this.notEmpty = false;
        this.errorMessages.push("Morate uneti sva polja!");
      }

      if(!regexPIB.test(this.formPIB)){
        this.pibOK = false;
        this.errorMessages.push("Neispravan PIB");
      }

      if(this.notEmpty && this.pibOK){
        orderer = {
          firstName: this.formFirstName,
          lastName: this.formLastName,
          phone: this.formPhone,
          email: this.formEmail,
          businessName: this.formBusinessName,
          businessAddress: this.formBusinessAddress,
          pib: this.formPIB,
          companyNumber: this.formCompanyNumber
        }
      }
    }
    else{
      orderer = this.orderersSearch.find( (company) => company.username == username);
    }

    console.log(orderer);
    if(orderer){
      const data = {
        orderer: orderer,
        companyUsername: this.user.username
      }
      this.companyService.addOrdererToCompany(data).subscribe( (resp: any) => {
        if(resp.username){
          localStorage.setItem("user", JSON.stringify(resp));
          this.user = JSON.parse(localStorage.getItem("user"));
          // window.location.reload();
        }
      })
    }
  }
}
