import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminUsersViewComponent } from './admin-users-view/admin-users-view.component';
import { GenerateInvoiceComponent } from './generate-invoice/generate-invoice.component';
import { GoodsAndServicesComponent } from './goods-and-services/goods-and-services.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { OrderersComponent } from './orderers/orderers.component';
import { ProductLayoutComponent } from './product-layout/product-layout.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { ReportsComponent } from './reports/reports.component';
import { TableLayoutComponent } from './table-layout/table-layout.component';
import { UsersInvoicesComponent } from './users-invoices/users-invoices.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'admin/login', component: AdminLoginComponent},
  {path: 'admin/users', component: AdminUsersViewComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'orderers', component: OrderersComponent},
  {path: 'goodsAndServices', component: GoodsAndServicesComponent},
  {path: 'productLayout', component: ProductLayoutComponent},
  {path: 'generateInvoice', component: GenerateInvoiceComponent},
  {path: 'tableLayout', component: TableLayoutComponent},
  {path: 'reports', component: ReportsComponent},
  {path: 'invoices', component: UsersInvoicesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
