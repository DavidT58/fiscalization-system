import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  selectedDate;
  company;
  total: Number;
  pdv: Number;
  error: String = "Nema racuna za izabrani dan";

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.company = JSON.parse(localStorage.getItem("user"));
  }

  getReport(){
    let data = {
      date: this.selectedDate
    }
    this.error = '';
    this.companyService.getReport(this.company.username, data).subscribe( (resp: any) => { 
      if(resp.success){
        if(this.company.pdv){
          this.total = resp.total * 0.8;
          this.pdv = resp.total * 0.2;
        }
        else{
          this.total = resp.total
        }
      }
      else{
        this.error = resp.message;
      }
    })

  }

}
