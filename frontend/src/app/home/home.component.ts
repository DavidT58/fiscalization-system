import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  user;
  companies;
  products;
  searchTerm = "";
  productsSearch;

  invoices;

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.companyService.getCompanies().subscribe( resp => {
      this.companies = resp;
    })
    this.companyService.getProducts().subscribe( resp => {
      this.products = resp;
      this.productsSearch = this.products;
    })
    this.companyService.getInvoices().subscribe( resp => {
      this.invoices = resp;
      console.log(resp)
    })
  }

  searchProducts(){
    const matcher = new RegExp(`${this.searchTerm}`, 'g');
    this.productsSearch = this.products.filter(product => product.productName.match(matcher));
  }

}
