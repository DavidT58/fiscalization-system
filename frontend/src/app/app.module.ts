import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgxPaginationModule} from 'ngx-pagination';
import {MatTabsModule} from '@angular/material/tabs';
// import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatRadioModule} from '@angular/material/radio';
import {MatDialogModule} from "@angular/material/dialog";
import {HttpClientModule} from '@angular/common/http';
// import { DragAndDropModule } from 'angular-draggable-droppable';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MdbCheckboxModule } from 'mdb-angular-ui-kit/checkbox';
import { MdbCollapseModule } from 'mdb-angular-ui-kit/collapse';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminUsersViewComponent } from './admin-users-view/admin-users-view.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { ProfileComponent } from './profile/profile.component';
import { OrderersComponent } from './orderers/orderers.component';
import { GoodsAndServicesComponent } from './goods-and-services/goods-and-services.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductLayoutComponent } from './product-layout/product-layout.component';
import { ProductLayoutDialogComponent } from './product-layout-dialog/product-layout-dialog.component';
import { GenerateInvoiceComponent } from './generate-invoice/generate-invoice.component';
import { TableLayoutComponent } from './table-layout/table-layout.component';
import { ReportsComponent } from './reports/reports.component';
import { UsersInvoicesComponent } from './users-invoices/users-invoices.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AdminUsersViewComponent,
    AdminLoginComponent,
    ProfileComponent,
    OrderersComponent,
    GoodsAndServicesComponent,
    ProductLayoutComponent,
    ProductLayoutDialogComponent,
    GenerateInvoiceComponent,
    TableLayoutComponent,
    ReportsComponent,
    UsersInvoicesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MdbCollapseModule,
    MdbCheckboxModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTabsModule,
    // MatSelectModule,
    MatRadioModule,
    MatPaginatorModule,
    MatDialogModule,
    // DragAndDropModule
    DragDropModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
