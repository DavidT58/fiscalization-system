import { Product } from "./product";
import { User } from "./user";

export class Invoice{
    products: Array<Product>;
    dateCreated: String;
    total: Number;
    company: String;

    customerFirstName: String;
    customerLastName: String;

    paymentMethod: String;

    idCardNumber: String;

    slipNumber: String;

    orderer: String;
};