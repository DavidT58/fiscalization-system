export class Product{
    
    //required
    productCode: String;
    productName: String;
    unitOfMeasure: String;
    taxRate: String;
    type: String;

    //additional
    manufacturerCountry: String;
    foreignName: String;
    barcodeNumber: String;
    manufacturerName: String;
    customsTariff: Number;
    ecoTax: Boolean;
    excise: Boolean;
    preferredMinStock: Number;
    preferredMaxStock: Number;
    description: String;
    declaration: String;
    
    imageUrl: String;
    
    warehouse: String;
    purchasePriceRSD: Number;
    salesPriceRSD: Number;
    currentStock: Number;
    
    amount: Number = 1;
};