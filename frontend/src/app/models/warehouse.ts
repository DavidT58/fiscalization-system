export class Warehouse{
    company: String;
    warehouseID: Number;
    warehouseName: String;
    products: Array<Object>;
};