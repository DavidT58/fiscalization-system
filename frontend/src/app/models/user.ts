export class User {
    firstName: string = "";
    lastName: string = "";
    username: string = "";
    phone: string = "";
    email: string = "";
    password: string = "";
    // "P" - "Prodavnica", "U" - "Ugostitelj"
    type: string = "";
}