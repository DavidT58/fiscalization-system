import { Product } from "./product";

export class Table{
    status: String;
    name: String;
    productsOnBill: Array<Product>;
    total: Number;
};