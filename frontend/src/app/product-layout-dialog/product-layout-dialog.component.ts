import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-product-layout-dialog',
  templateUrl: './product-layout-dialog.component.html',
  styleUrls: ['./product-layout-dialog.component.scss']
})
export class ProductLayoutDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ProductLayoutDialogComponent>,
    private companyService: CompanyService,
  ) { }

  products = [];
  productsSearch = [];
  categories = [];
  selectedCategory = [];
  p = 1;
  term = "";
  errors = [];

  ngOnInit(): void {
    let username = JSON.parse(localStorage.getItem('user'))['username']
    this.companyService.getCompanyProducts(username).subscribe( (resp: any) => {
      this.productsSearch = this.products = resp;
    })
    this.companyService.getCategories().subscribe( (resp : any) => {
      this.categories = resp;
    })
  }

  searchProducts(){
    const matcher = new RegExp(`^${this.term}`, 'g');
    this.productsSearch = this.products.filter(product => product.product.productName.match(matcher));
  }

  addCategoryToProduct(product, category){
    if(this.selectedCategory[category]){
      const data = {
        company: JSON.parse(localStorage.getItem('user'))['username'],
        product: product,
        category: this.selectedCategory[category]
      }
      this.companyService.addCategoryToProduct(data).subscribe( (resp : any) => {
        if(!resp.success){
          alert("Artikal se vec nalazi u kategoriji " + product.category)
        }
        else{
          this.products = resp.products;
          window.location.reload();
        }
      })
    }
  }
}
