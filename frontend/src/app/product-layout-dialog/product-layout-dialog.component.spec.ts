import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductLayoutDialogComponent } from './product-layout-dialog.component';

describe('ProductLayoutDialogComponent', () => {
  let component: ProductLayoutDialogComponent;
  let fixture: ComponentFixture<ProductLayoutDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductLayoutDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductLayoutDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
