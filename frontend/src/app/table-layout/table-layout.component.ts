import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from '../services/company.service';


@Component({
  selector: 'app-table-layout',
  templateUrl: './table-layout.component.html',
  styleUrls: ['./table-layout.component.scss'],
})
export class TableLayoutComponent implements OnInit {

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"))
  }

  user;
  
  addTable(){
    if(this.user.tables == undefined){
      this.user.tables = [];
    }
    if(this.user.tables.length < 8){
      let s = (this.user.tables.length + 1).toString();
      this.user.tables.push({status: 'SLOBODAN', name: s})
      let data = {
        tables: this.user.tables
      }
      this.companyService.updateTables(this.user.username, data).subscribe( (resp) => {
        this.user = resp;
        localStorage.setItem("user", JSON.stringify(this.user));
        console.log(this.user.tables)
      })
    }
    else{
      alert("Nema mesta")
    }
    
  }

  // computeDragRenderPos(pos, dragRef) {
  //   return {x: Math.floor(pos.x / 50) * 50, y: Math.floor(pos.y / 50) * 50};
  // }

  // dragEnd(event){
  //   console.log(event.);
  // }

  // test(event){
  //   console.log(event);
  // }


}
