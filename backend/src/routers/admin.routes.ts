import express from 'express';
import multer from 'multer';
import { AdminController } from '../controllers/admin.controller';

const adminRouter = express.Router();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')},
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.' + file.mimetype.split('/')[1])
    }
})

const upload = multer({ storage: storage });

adminRouter.post('/registerCompany', upload.single('image'), (req, res, next) => {
    req.body.imagePath = req.file.path;
    next();
});

adminRouter.route('/getPendingCompanies').get(
    (req, res) => new AdminController().getPendingCompanies(req, res)
)

adminRouter.route('/getCompanies').get(
    (req, res) => new AdminController().getCompanies(req, res)
)

adminRouter.route('/approveCompany/:username').post(
    (req, res) => new AdminController().approveCompany(req, res)
)

adminRouter.route('/rejectCompany/:username').post(
    (req, res) => new AdminController().rejectCompany(req, res)
)

adminRouter.route('/registerCompany').post(
    (req, res) => new AdminController().registerCompany(req, res)
)

adminRouter.route('/registerUser').post(
    (req, res) => new AdminController().registerUser(req, res)
)


export default adminRouter;