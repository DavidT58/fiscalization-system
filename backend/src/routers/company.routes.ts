import express from 'express';
import multer from 'multer';
import { CompanyController } from '../controllers/company.controller';

const companyRouter = express.Router();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')},
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.' + file.mimetype.split('/')[1])
    }
})

const upload = multer({ storage: storage });

companyRouter.post('/:company/addProduct', upload.single('image'), (req, res, next) => {
    if(req.file){
        req.body.imagePath = req.file.path;
    }
    next();
});

companyRouter.route('/').get(
    (req, res) => new CompanyController().getCompanies(req, res)
)

companyRouter.route('/submitCompanyDetails').post(
    (req, res) => new CompanyController().submitCompanyDetails(req, res)
)

companyRouter.route('/:company/removeWarehouse').post(
    (req, res) => new CompanyController().removeWarehouse(req, res)
)

companyRouter.route('/getCompanyByPIB/:pib').get(
    (req, res) => new CompanyController().getCompanyByPIB(req, res)
)

companyRouter.route('/getOrdererByID/:id').get(
    (req, res) => new CompanyController().getOrdererByID(req, res)
)

companyRouter.route('/addOrdererToCompany').post(
    (req, res) => new CompanyController().addOrdererToCompany(req, res)
)

companyRouter.route('/:company/getCompanyProducts').get(
    (req, res) => new CompanyController().getCompanyProducts(req, res)
)

companyRouter.route('/:company/getCompanyWarehouses').get(
    (req, res) => new CompanyController().getCompanyWarehouses(req, res)
)

companyRouter.route('/:company/addProduct').post(
    (req, res) => new CompanyController().addProduct(req, res)
)

companyRouter.route('/getCategories').get(
    (req, res) => new CompanyController().getCategories(req, res)
)

companyRouter.route('/addCategory').post(
    (req, res) => new CompanyController().addCategory(req, res)
)

companyRouter.route('/categories/:category/addSubCategory').post(
    (req, res) => new CompanyController().addSubCategory(req, res)
)

companyRouter.route('/addCategoryToProduct').post(
    (req, res) => new CompanyController().addCategoryToProduct(req, res)
)

companyRouter.route('/:company/generateInvoice').post(
    (req, res) => new CompanyController().generateInvoice(req, res)
)

companyRouter.route('/:company/updateTables').post(
    (req, res) => new CompanyController().updateTables(req, res)
)

companyRouter.route('/:company/getReport').post(
    (req, res) => new CompanyController().getReport(req, res)
)

companyRouter.route('/getUserInvoices/:username').get(
    (req, res) => new CompanyController().getUserInvoices(req, res)
)

companyRouter.route('/products').get(
    (req, res) => new CompanyController().getProducts(req, res)
)

companyRouter.route('/invoices').get(
    (req, res) => new CompanyController().getInvoices(req, res)
)

export default companyRouter;
