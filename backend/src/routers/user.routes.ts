import express from 'express';
import multer from 'multer';
import { UserController } from '../controllers/user.controller';

const userRouter = express.Router();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')},
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.' + file.mimetype.split('/')[1])
    }
})

const upload = multer({ storage: storage });

userRouter.post('/register', upload.single('image'), (req, res, next) => {
    req.body.imagePath = req.file.path;
    next();
});

userRouter.route('/login').post(
    (req, res) => new UserController().login(req, res)
)

userRouter.route('/adminLogin').post(
    (req, res) => new UserController().adminLogin(req, res)
)

userRouter.route('/register').post(
    (req, res) => new UserController().register(req, res)
)

userRouter.route('/resetPassword').post(
    (req, res) => new UserController().resetPassword(req, res)
)

export default userRouter;
