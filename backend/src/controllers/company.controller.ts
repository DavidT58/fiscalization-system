import * as express from 'express';
import mongoose from 'mongoose';
import Company from '../models/company';
import User from '../models/user';
import Admin from '../models/admin';
import Orderer from '../models/orderer';
import Warehouse from '../models/warehouse';
import Product from '../models/product';
import WarehousesProducts from '../models/warehouses_products';
import Category from '../models/category';
import Invoice from '../models/invoice';

export class CompanyController{
    
    submitCompanyDetails = (req: express.Request, res: express.Response) => {
        let username = req.body.username;
        
        let category = req.body.category;
        let activityCodes = req.body.activityCodes;
        let pdv = req.body.pdv;
        let bankAccounts = req.body.bankAccounts;
        let warehouses = req.body.warehouses;
        let fiscalRegisters = req.body.fiscalRegisters;

        Company.findOneAndUpdate(
            {"username": username}, 
            {
                "category": category, 
                "activityCodes": activityCodes, 
                "pdv": pdv,
                "bankAccounts": bankAccounts,
                'warehouses': warehouses,
                "fiscalRegisters": fiscalRegisters
            },
            { returnDocument: 'after' },
            (err, comp) => {
                if(comp == undefined){
                    res.json({companyNotFound: true});
                }
                else{
                    warehouses.forEach( (wh, index, array) => {
                        Warehouse.exists(
                            {warehouseID: wh.warehouseID},
                            (err, doc) => {
                                if(!doc){
                                    let newWarehouse = new Warehouse(wh);
                                    newWarehouse.save();
                                }
                            }
                        )
                    })
                    res.json(comp);
                }
            }
        )
    }

    removeWarehouse = (req: express.Request, res: express.Response) => {
        let company = req.params.company;

        Warehouse.findOneAndRemove(
            {warehouseID: req.body.warehouseID},
            (err, doc) => {
                if(err) console.log(err);
                // console.log(doc);
                Company.findOneAndUpdate(
                    {username: company},
                    { $pull: { warehouses: {warehouseID: req.body.warehouseID} } },
                    { returnDocument: 'after' },
                    (err, doc) => {
                        if(err) console.log(err);
                        if(doc){
                            res.json(doc)
                        }
                        else{
                            res.json({companyNotFound: true});
                        }
                    }
                )
            }
        )
    }

    getCompanyByPIB = (req: express.Request, res: express.Response) => {

        Company.find(
            {"pib": { $regex: req.params.pib }},
            (err, data) => {
                if(err) console.log(err);
                // console.log(data);
                res.json(data);
            }
        );
    }

    getOrdererByID = (req: express.Request, res: express.Response) => {
        Orderer.find(
            {"_id": { $regex: req.params.id }},
            (err, data) => {
                if(err) console.log(err);
                // console.log(data);
                res.json(data);
            }
        )
    }

    getCompanyByID = (id) => {
        Company.find(
            {"_id": id },
            (err, data) => {
                if(err) console.log(err);
                return data;
            }
        );
    }

    addOrdererToCompany = (req: express.Request, res: express.Response) => {
        let companyUsername = req.body.companyUsername;

        let ordererReq = req.body.orderer;
        delete ordererReq['_id'];

        let orderer = new Orderer(ordererReq);

        orderer.save(
            (err, doc) => {
                if(err) console.log(err);
            }
        );
        
        Company.findOneAndUpdate(
            {"username": companyUsername},
            { $push: { orderers: orderer }},
            {
                returnDocument: 'after'
            }
        ).populate('orderers').exec(
            (err, company) => {
                if(err) console.log(err);
                res.json(company);
            }
        );
    }

    getCompanyProducts = async (req: express.Request, res: express.Response) => {
        let company = req.params.company;

        let products = await WarehousesProducts.find(
            {'company': company}
        ).populate('product').populate('warehouse');

        res.json(products);
        
    }

    addProduct = async (req: express.Request, res: express.Response) => {
        let company = req.params.company;
        let warehouseID = req.body.warehouseID;

        let warehouse = await Warehouse.findOne({'warehouseID': warehouseID});
        let product = new Product(
            {
                _id: new mongoose.Types.ObjectId(),
                company: company,
                warehouse: warehouse._id,

                productCode: req.body.productCode,
                productName: req.body.productName,
                unitOfMeasure: req.body.unitOfMeasure,
                taxRate: req.body.taxRate,
                type: req.body.type,
    
                purchasePriceRSD: req.body.purchasePriceRSD,
                salesPriceRSD: req.body.salesPriceRSD,
            }
        );
        
        product.imageUrl = (req.body.imageUrl) ? req.body.imageUrl : 'uploads/product-icon.png';
        product.save();
        
        WarehousesProducts.findOneAndUpdate(
            {
                'product': product._id,
                'warehouse': warehouse._id
            },
            {
                $inc: { 'currentStock': 1, },
                'minWantedStock': 1,
                'maxWantedStock': 10,
                'company': company
            },
            {
                new: true,
                upsert: true
            },
            (err, doc) => {
                if(err) console.log(err);
                if(doc){
                    res.json({success: true})
                }
                else{
                    res.json({success: false})
                }
            }
        )
    }

    getCategories = (req: express.Request, res: express.Response) => {
        Category.find(
            (err, categories) => {
                if(err) console.log(err);
                res.json(categories);
            }
        )
    }

    addCategory = (req: express.Request, res: express.Response) => {
        let categoryName = req.body.categoryName;

        if(categoryName == undefined || categoryName == ""){ res.json({success: false})}

        let newCategory = new Category({
            name: categoryName
        })

        newCategory.save((err, doc) => {
            if(err) console.log(err);
            Category.find(
                (err, categories) => {
                    if(err) console.log(err);
                    res.json(categories);
                }
            )
        });
    }

    addSubCategory = (req: express.Request, res: express.Response) => {

    }

    addCategoryToProduct = (req: express.Request, res: express.Response) => {
        let company = req.body.company;
        let product = req.body.product;
        let category = req.body.category;

        Product.findOne(
            {'_id': product._id, 'category': { $ne: null }},
            (err, doc) => {
                if(err) console.log(err);
                if(doc) {
                    WarehousesProducts.find(
                        { 'company': company }
                    ).populate('product').populate('warehouse')
                    .exec((err, doc) => {
                        res.json({success: false, products: doc});
                    })
                }
                else{
                    Product.findOneAndUpdate(
                        {'_id': product._id},
                        {'category': category},
                        { returnDocument: 'after' },
                        (err, doc) => {
                            if(err) console.log(err);
                            WarehousesProducts.find(
                                { 'company': company }
                            ).populate('product').populate('warehouse')
                            .exec((err, doc) => {
                                res.json({success: true, products: doc});
                            })
                        }
                    )
                }
            }
        )
    }

    getCompanyWarehouses = (req: express.Request, res: express.Response) => {
        let company = req.params.company;

        Warehouse.find(
            {'company': company}
        ).exec( (err, doc) => {
            res.json(doc);
        })
    }

    generateInvoice = async (req: express.Request, res: express.Response) => {
        let company = req.params.company;

        let productsFromRequest = req.body.products;


        let warehouse = req.body.warehouseId;

        let success = true;

        let wh = await Warehouse.findOne({'_id': warehouse});

        let cmp = await Company.findOne({'username': company});
        
        (await WarehousesProducts.find({'warehouse': warehouse})).forEach( (i) => {
            productsFromRequest.forEach( (j) => {
                if(i.product._id == j._id){
                    WarehousesProducts.findOneAndUpdate(
                        {'product': j._id, 'currentStock': {$gte: j.amount}},
                        {$inc: {'currentStock': -j.amount}},
                        { returnDocument: 'after' },
                        (err, doc) => {
                            if(err) console.log(err);

                            if(doc == null) success = false;
                        }
                    )
                    console.log("Updated product id: " + j._id);
                }
            });
        });

        let c;

        if(success){
            let productArr = [];
        
            productsFromRequest.forEach(element => {
                let o = {
                    _id: element._id,
                    amount: element.amount
                }
                productArr.push(o);
            });

            let newInvoice = new Invoice({
                'company': company,
                'businessName': cmp.businessName,
                'total': req.body.total,
                'dateCreated': req.body.dateCreated,
                'location': wh.warehouseName,
                'products': productArr,
                'paymentMethod': req.body.paymentMethod,
                'customerFirstName': req.body.customerFirstName,
                'customerLastName': req.body.customerLastName,
                'idCardNumber': req.body.idCardNumber,
                'slipNumber': req.body.slipNumber,
                'orderer': req.body.orderer
            })

            newInvoice.save();

            c = await Company.findOneAndUpdate(
                {'username': company},
                {$push: {'invoices': newInvoice._id}},
                { returnDocument: 'after' }
            ).populate({
                path : 'invoices',
                populate : {
                  path : 'products._id'
                }
            });
        }
        else{
            c = await Company.findOne({'username': company}).populate({
                path : 'invoices',
                populate : {
                  path : 'products._id'
                }
            });
        }

        let pRet = await WarehousesProducts.find(
            {'company': company}
        ).populate('product').populate('warehouse');

        res.json({
            'company': c,
            'products': pRet,
            'success': success
        });
    }

    updateTables = (req: express.Request, res: express.Response) => {
        let company = req.params.company;

        let tables = req.body.tables;

        Company.findOneAndUpdate(
            {'username': company},
            {$set: {'tables': tables}},
            { returnDocument: 'after' },
            (err, doc) => {
                if(err) console.log(err);
                res.json(doc);
            }
        )
    }

    getReport = async (req: express.Request, res: express.Response) => {
        let company = req.params.company;
        let date = new Date(req.body.date);

        function sameDay(d1, d2) {
            return d1.getFullYear() === d2.getFullYear() &&
                d1.getMonth() === d2.getMonth() &&
                d1.getDate() === d2.getDate();
        }

        let invoices = await Invoice.find({'company': company});

        let invoicesThatCount = [];

        let total = 0;

        invoices.forEach( (inv) => {
            let d1 = new Date(inv.dateCreated);
            if(sameDay(d1, date)){
                total += inv.total;
                invoicesThatCount.push(inv);
            }
        } )

        if(invoicesThatCount.length == 0){
            res.json({
                success: false,
                message: "Nema racuna za izabrani dan"
            })
        }
        else{
            console.log(invoices)
            res.json({
                success: true,
                total: total
            });
        }
    }

    getCompanies = async (req: express.Request, res: express.Response) => {
        let companies = await Company.find();

        res.json(companies);
    }

    getUserInvoices = async (req: express.Request, res: express.Response) => {
        let username = req.params.username;

        let user = await User.findOne({'username': username});

        let invoices = await Invoice.find({'idCardNumber': user.idNumber}).populate('products._id');
        
        res.json(invoices);
    }

    getProducts = async (req: express.Request, res: express.Response) => {
        let p = await Product.find().populate('warehouse');
        res.json(p);
    }

    getInvoices = async (req: express.Request, res: express.Response) => {
        let i = await Invoice.find().populate('products._id');
        res.json(i);
    }
};