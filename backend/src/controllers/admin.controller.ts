import * as express from 'express';
import Company from '../models/company';
import User from '../models/user';

export class AdminController{

    createUser = (req: express.Request, res: express.Response) => {

    }

    createCompany = (req: express.Request, res: express.Response) => {
        
    }

    getPendingCompanies = (req: express.Request, res: express.Response) => {
        Company.find({'status': 'Pending'}, (err: any, companies: any) => {
            if(err) console.log(err);
            // console.log(companies);
            res.json(companies);
        })
    }

    getCompanies = (req: express.Request, res: express.Response) => {
        Company.find({$or: [{ 'status': 'Deactivated'}, {'status': 'Activated'}]}, (err: any, companies: any) => {
            if(err) console.log(err);
            // console.log(companies);
            res.json(companies);
        })
    }

    approveCompany = async (req: express.Request, res: express.Response) => {

        let comp = await Company.findOneAndUpdate({'username': req.params.username}, {'status': 'Activated'});

        res.json(comp);
    }

    rejectCompany = async (req: express.Request, res: express.Response) => {
        let comp = await Company.findOneAndUpdate({'username': req.params.username}, {'status': 'Deactivated'});

        res.json(comp);
    }

    registerCompany = async (req: express.Request, res: express.Response) => {
        
        let company = new Company(req.body);

        company.type = "Company";
        company.status = "Activated";

        Company.findOne(

            { $or: [ {"username": company.username}, {"email": company.email} ]},

            (err, usr) => {
                if(err){
                    res.status(400).json({error: err});
                }
                if (usr){
                    res.status(200).json({exists: true});
                }
                else{
                    User.findOne(

                        { "username": company.username },
            
                        (err, usr) => {
                            if(err){
                                res.status(400).json({error: err});
                            }
                            
                            if (usr){
                                res.status(200).json({exists: true});
                            }
                            else{
                                company.save().then(
                                    (doc: any) => {
                                        if (doc) {
                                            res.status(200).json({registered: true});
                                        } else {
                                            res.status(200).json({registered: false});
                                        }
                                    }
                                );
                            }
                        }
                    )
                }
            }
        )
    }

    registerUser = async (req: express.Request, res: express.Response) => {
        
        let user = new User(req.body);

        user.type = "User";

        Company.findOne(

            { "username": user.username },

            (err, usr) => {
                if(err){
                    res.status(400).json({error: err});
                }
                if (usr){
                    res.status(200).json({exists: true});
                }
                else{
                    User.findOne(

                        { "username": user.username },
            
                        (err, usr) => {
                            if(err){
                                res.status(400).json({error: err});
                            }
                            
                            if (usr){
                                res.status(200).json({exists: true});
                            }
                            else{
                                user.save().then(
                                    (doc: any) => {
                                        if (doc) {
                                            res.status(200).json({registered: true});
                                        } else {
                                            res.status(200).json({registered: false});
                                        }
                                    }
                                );
                            }
                        }
                    )
                }
            }
        )
    }
};