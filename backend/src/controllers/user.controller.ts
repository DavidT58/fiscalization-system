import * as express from 'express';
import Company from '../models/company';
import User from '../models/user';
import Admin from '../models/admin';

export class UserController{

    login = (req: express.Request, res: express.Response) => {
        let username = req.body.username;
        let password = req.body.password;

        Company.findOne(
            {'username': username, 'password': password, 'status': 'Activated'}
        ).populate('orderers._id').exec(
            (err, company) => {
                if(err) console.log(err);
                if(company){
                    res.json(company);
                }
                else{
                    User.findOne({'username': username, 'password': password}, (err: any, user: any) => {
                        if(err) console.log(err);
                        if (user){
                            res.json(user);
                        }
                        else{
                            res.json({notFound: true})
                        }
                    })
                }
            }
        );
    }

    adminLogin = (req: express.Request, res: express.Response) => {
        let username = req.body.username;
        let password = req.body.password;
        Admin.findOne(
            {'username': username, 'password': password}, 
            
            (err: any, user: any) => {
                if(err) console.log(err);
                if (user){
                    console.log('logged in');
                    res.json({
                        username: user.username, 
                        type: user.type,
                        firstName: user.firstName,
                        lastName: user.lastName
                    });
                }
                else{
                    res.json({notFound: true})
                }
            }
        )
    }

    register = (req: express.Request, res: express.Response) => {

        let company = new Company(req.body);

        company.type = "Company";
        company.status = "Pending";

        Company.findOne(

            { $or: [ {"username": company.username}, {"email": company.email} ]},

            (err, usr) => {
                if(err){
                    res.status(400).json({error: err});
                }
                if (usr){
                    res.json({exists: true});
                }
                else{
                    User.findOne(

                        { "username": company.username },
            
                        (err, usr) => {
                            if(err){
                                res.status(400).json({error: err});
                            }
                            
                            if (usr){
                                res.json({exists: true});
                            }
                            else{
                                company.save().then(
                                    (doc: any) => {
                                        if (doc) {
                                            res.json({
                                                username: company.username, 
                                                type: company.type,
                                                firstName: company.firstName,
                                                lastName: company.lastName
                                            });
                                        } else {
                                            res.json({registered: false});
                                        }
                                    }
                                );
                            }
                        }
                    )
                }
            }
        )
    }

    resetPassword = async (req: express.Request, res: express.Response) => {
        let username = req.body.username;
        let newPassword = req.body.newPassword;
        let oldPassword = req.body.oldPassword;
        let type = req.body.type;

        switch(type){
            case "User": 
                let user = await User.findOneAndUpdate({"username": username, "password": oldPassword}, {"password": newPassword});
                if(user){
                    res.json({"oldPasswordCorrect": true});
                }
                else{
                    res.json({"oldPasswordCorrect": false});
                }
                break;
            case "Company":
                let company = await Company.findOneAndUpdate({"username": username, "password": oldPassword}, {"password": newPassword});
                if(company){
                    res.json({"oldPasswordCorrect": true});
                }
                else{
                    res.json({"oldPasswordCorrect": false});
                }
                break;

            case "Admin":
                let admin = await Admin.findOneAndUpdate({"username": username, "password": oldPassword}, {"password": newPassword});
                if(admin){
                    res.json({"oldPasswordCorrect": true});
                }
                else{
                    res.json({"oldPasswordCorrect": false});
                }
                break;
        }
    }
}
