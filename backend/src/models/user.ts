import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let User = new Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    phone: {
        type: String
    },
    idNumber: {
        type: String
    },
    username: {
        type: String
    },
    password: {
        type: String
    },
    type: {
        type: String
    }
})

export default mongoose.model('User', User, 'users');
