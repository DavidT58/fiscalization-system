import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let WarehousesProducts = new Schema({
    company: String,
    product: {
        type: Schema.Types.ObjectId, ref: 'Product'
    },
    warehouse: {
        type: Schema.Types.ObjectId, ref: 'Warehouse'
    },
    currentStock: {
        type: Number,
        default: 0
    },
    minWantedStock: Number,
    maxWantedStock: Number
});

export default mongoose.model('WarehousesProducts', WarehousesProducts, 'warehouses_products');