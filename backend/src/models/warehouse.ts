import mongoose from 'mongoose';
import Product from './product';

const Schema = mongoose.Schema;

let Warehouse = new Schema({
    company: String,
    warehouseID: Number,
    warehouseName: String
});

export default mongoose.model('Warehouse', Warehouse, 'warehouses');