import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Invoice = new Schema({
    company: String,
    businessName: String,
    total: Number,
    dateCreated: String,
    location: String,
    products: [
        {
            _id: {
                type: Schema.Types.ObjectId, 
                ref: 'Product'
            },
            amount: Number
        }
    ],

    paymentMethod: String,

    customerFirstName: String,
    customerLastName: String,

    idCardNumber: String,

    slipNumber: String,

    orderer: String
});

export default mongoose.model('Invoice', Invoice, 'invoices');