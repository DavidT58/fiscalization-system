import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Orderer = new Schema({
    firstName: String,
    lastName: String,
    phone: String,
    email: String,
    businessName: String,
    businessAddress: String,
    pib: String,
    companyNumber: String,
    numOfDaysForPayment: Number,
    rebate: Number
});

export default mongoose.model('Orderer', Orderer, 'orderers');