import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Product = new Schema({
    
    _id: Schema.Types.ObjectId,

    company: String,
    // warehouseID: Number,
    warehouse: {
        type: Schema.Types.ObjectId, 
        ref: 'Warehouse'
    },
    
    //required
    productCode: String,
    productName: String,
    unitOfMeasure: String,
    taxRate: String,
    type: String,
    //additional
    manufacturerCountry: String,
    foreignName: String,
    barcodeNumber: String,
    manufacturerName: String,
    customsTariff: Number,
    ecoTax: Boolean,
    excise: Boolean,
    preferredMinStock: Number,
    preferredMaxStock: Number,
    description: String,
    declaration: String,
    
    imageUrl: String,
    
    // locationName: String, - warehouse field
    purchasePriceRSD: Number,
    salesPriceRSD: Number,

    category: String,
    subCategory: String
});

export default mongoose.model('Product', Product, 'products');