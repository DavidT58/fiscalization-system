import mongoose from 'mongoose';
import Orderer from './orderer';
import Product from './product';
import Warehouse from './warehouse';

const Schema = mongoose.Schema;

let Company = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    businessName: String,
    businessAddress: String,
    pib: String,
    companyNumber: String,
    username: String,
    password: String,
    imagePath: String,
    status: String,
    type: String,
    category: String,
    activityCodes: [String],
    pdv: Boolean,
    bankAccounts: [
        {
            bankName: String,
            accountNumber: String
        }
    ],
    warehouses: [
        {
            warehouseID: Number,
            warehouseName: String
        }
    ],
    fiscalRegisters: [
        {
            objectLocation: String,
            registerType: String
        }
    ],
    orderers: [
        Orderer.schema
    ],
    invoices: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Invoice'
        }
    ],
    tables: [
        {
            name: String,
            status: String,
        }
    ]
})

export default mongoose.model('Company', Company, 'companies');
