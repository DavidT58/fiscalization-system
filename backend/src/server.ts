import express, { Express, RequestHandler } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import userRouter from './routers/user.routes';
import adminRouter from './routers/admin.routes';
import companyRouter from './routers/company.routes';

const app = express();

app.use(cors());
app.use(bodyParser.json() as RequestHandler);
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/uploads', express.static('uploads'));


mongoose.connect('mongodb://root:root@127.0.0.1:27017/pia?authSource=admin', {
    // useNewUrlParser: true,
    // useUnifiedTopology: true
});
const connection = mongoose.connection;
connection.once('open', () => {
    console.log('db connection ok');
});

const router = express.Router();

router.use('/users', userRouter);
router.use('/admin', adminRouter);
router.use('/companies', companyRouter);

app.use('/', router);

app.listen(8000, () => console.log(`Express server running on port 8000`));